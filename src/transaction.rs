use std::{collections::HashMap, fmt::Display};

use idb::Query;
use serde::{de::DeserializeOwned, Serialize};
use wasm_bindgen::JsValue;

use crate::{
    keyval::{bincode_from_js, from_js, to_js},
    Error,
};

pub struct Transaction {
    tx: Option<idb::Transaction>,
    store: idb::ObjectStore,
}

impl Transaction {
    /// Create a new transaction.
    pub(crate) fn new(tx: idb::Transaction, store: idb::ObjectStore) -> Self {
        Self {
            tx: Some(tx),
            store,
        }
    }

    /// Get the value for a given key.
    pub async fn get<K, V>(&mut self, key: K) -> Result<V, Error>
    where
        K: Display,
        V: DeserializeOwned,
    {
        let key = key.to_string();
        let val = self
            .store
            .get(to_js(&key)?)
            .await?
            .ok_or(Error::NoValue(key))?;
        from_js(val)
    }

    /// Remove a given key and its value.
    pub async fn remove<K: Display>(&mut self, key: K) -> Result<(), Error> {
        let key = key.to_string();
        self.store.delete(Query::Key(to_js(key)?)).await?;
        Ok(())
    }

    /// Remove an array of keys and their values.
    pub async fn remove_many<K, T>(&mut self, key_arr: T) -> Result<(), Error>
    where
        T: Iterator<Item = K>,
        K: Display,
    {
        for key in key_arr {
            let key = key.to_string();
            self.store.delete(Query::Key(to_js(key)?)).await?;
        }
        Ok(())
    }

    /// Save a key and value pair int the DB.
    pub async fn set<K, V>(&mut self, key: K, val: V) -> Result<(), Error>
    where
        K: Display,
        V: Serialize,
    {
        self.store
            .put(&to_js(val)?, Some(&to_js(key.to_string())?))
            .await?;
        Ok(())
    }

    /// Delete all values from the database.
    pub async fn delete_all(&mut self) -> Result<(), Error> {
        self.store.clear().await?;
        Ok(())
    }

    /// Gets the size of the database.
    pub async fn size(&mut self) -> Result<u64, Error> {
        let mut size = 0;
        let keys = self.store.get_all_keys(None, None).await?;
        for key in keys {
            let value = self.store.get(key).await?.ok_or(Error::NoValueNoKey)?;
            let value = bincode_from_js(value);
            size += value.len() as u64;
        }
        Ok(size)
    }

    /// Exports all values as a byte array.
    pub async fn export(&mut self) -> Result<Vec<u8>, Error> {
        // The blob just takes the raw bincode saved indexeddb.
        // So keys and values are left as bincode without serialising them into
        // proper values.
        let mut blob = HashMap::new();
        let keys = self.store.get_all_keys(None, None).await?;
        for key in keys {
            let value = self
                .store
                .get(key.clone())
                .await?
                .ok_or(Error::NoValueNoKey)?;
            let value = bincode_from_js(value);
            let key = bincode_from_js(key);
            blob.entry(key).or_insert(value);
        }
        // The hashmap is serialised once more to ensure a single byte array is returned.
        let blob = bincode::serialize(&blob)?;
        Ok(blob)
    }

    /// Imports previously exported values.
    /// This action will clear the entire database.
    pub async fn import(&mut self, export: Vec<u8>) -> Result<(), Error> {
        let convert = |value: Vec<u8>| JsValue::from(js_sys::Uint8Array::from(&value[..]));
        let blob: HashMap<Vec<u8>, Vec<u8>> = bincode::deserialize(&export)?;
        for (key, value) in blob {
            let (key, value) = (convert(key), convert(value));
            self.store.put(&value, Some(&key)).await?;
        }
        Ok(())
    }

    /// Close the transaction without changing anything.
    pub async fn close(mut self) -> Result<(), Error> {
        Ok(self.tx.take().unwrap().done().await?)
    }

    /// Close the transaction and writing to results to the DB.
    pub async fn commit(mut self) -> Result<(), Error> {
        Ok(self.tx.take().unwrap().commit().await?)
    }
}
