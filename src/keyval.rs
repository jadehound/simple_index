use crate::Error;
use serde::{de::DeserializeOwned, Serialize};
use wasm_bindgen::JsValue;

pub fn to_js<V>(value: V) -> Result<JsValue, Error>
where
    V: Serialize,
{
    let code = js_to_bincode(value)?;
    let js_value = JsValue::from(js_sys::Uint8Array::from(&code[..]));
    Ok(js_value)
}

pub fn from_js<V>(value: JsValue) -> Result<V, Error>
where
    V: DeserializeOwned,
{
    let code = bincode_from_js(value);
    Ok(bincode::deserialize::<V>(&code)?)
}

pub fn bincode_from_js(value: JsValue) -> Vec<u8> {
    js_sys::Uint8Array::from(value).to_vec()
}

pub fn js_to_bincode<V>(value: V) -> Result<Vec<u8>, Error>
where
    V: Serialize,
{
    Ok(bincode::serialize(&value)?)
}
